#include <SoftwareSerial.h>
#include "uECC.h"
#include "Adafruit_NeoPixel.h"


// MEGA
/* #define BT_RX_PIN 51 */
/* #define BT_TX_PIN 53 */
/* #define BT_AT_PIN 47 */
/* #define BT_VC_PIN 2 */
/* #define BUZZER_PIN 12 */

// NANO
#define BT_RX_PIN 6
#define BT_TX_PIN 7
#define BT_AT_PIN 100
#define BT_VC_PIN 2
#define BUZZER_PIN A5
#define aHIGH 255
#define aLOW 0
#define N_LEDS 24
#define LEDS_PIN 5

SoftwareSerial BT(BT_RX_PIN, BT_TX_PIN);
Adafruit_NeoPixel LEDS(N_LEDS, LEDS_PIN);

void fill_circle(uint8_t r, uint8_t g, uint8_t b, uint16_t delay_time=0) {
  for (int i = 0; i< N_LEDS; ++i) {
    LEDS.setPixelColor(i, r, g, b);
    if (delay_time) {
      delay(delay_time);
      LEDS.show();
    }
  }
  LEDS.show();
}

void setup() {
  randomSeed(analogRead(0));

  Serial.begin(38400);
  BT.begin(38400);
  LEDS.begin();
  LEDS.setBrightness(25);

  pinMode(BT_VC_PIN, OUTPUT);
  pinMode(BT_AT_PIN, OUTPUT);
  pinMode(BUZZER_PIN, OUTPUT);

  reset();

  while (BT.available() > 0) BT.read();
  BT.write("AT+INIT\r\n");
  Serial.println("ready");
}

void reset() {
  analogWrite(BUZZER_PIN, aLOW);
  digitalWrite(BT_VC_PIN, LOW);
  digitalWrite(BT_AT_PIN, LOW);
  digitalWrite(BT_VC_PIN, HIGH);
}


#define FIRST 0x01
#define SECOND 0x02
#define NONCE_LENGTH 32

byte r;
byte data[NONCE_LENGTH];

void fill_random_bytes(uint8_t *data, uint8_t n) {
  // This probably is not cryptografically secure
  for (uint8_t i = 0; i < n; i++) {
    data[i] = random(256);
  }
}

void dump_byte_array(const byte *key, int size) {
  int i;
  char t[2];
  for (i = 0; i < size; i++) {
    sprintf(t, "%02x", key[i]);
    Serial.print(t);
  }
  Serial.print("\n");
}

void music_ok() {
  analogWrite(BUZZER_PIN, aHIGH);
  delay(100);
  analogWrite(BUZZER_PIN, aLOW);
  delay(100);
  analogWrite(BUZZER_PIN, aHIGH);
  delay(200);
  analogWrite(BUZZER_PIN, aLOW);
  delay(100);
  analogWrite(BUZZER_PIN, aHIGH);
  delay(300);
  analogWrite(BUZZER_PIN, aLOW);
}

void music_bad() {
  analogWrite(BUZZER_PIN, aHIGH);
  delay(500);
  analogWrite(BUZZER_PIN, aLOW);
}

// 256r1 ECC
uint8_t public_key[64] = {
  // Here goes your public key (EEC
  // X
  100, 236, 131, 189, 38, 80, 127, 112, 60, 134, 149, 149, 153, 103, 4, 101,
  170, 64, 31, 227, 220, 119, 17, 236, 219, 255, 29, 157, 30, 238, 44, 28,
  // Y
  6, 36, 228, 215, 49, 229, 55, 175, 184, 228, 2, 57, 134, 166, 2, 70, 153,
  80, 57, 157, 244, 159, 51, 151, 47, 246, 129, 122, 154, 12, 35, 56
};

byte signature[64];


bool ok;
// TODO: timeout?
bool read_bytes(byte *d, int size, bool check) {
  bool valid = true;
  byte k;
  char t[2];
  /* Serial.print("check="); */
  /* Serial.println(check); */
  /* Serial.print("read: "); */
  for (int i=0; i < size && valid; ++i) {
    while (BT.available() == 0);

    k = BT.read();
    sprintf(t, "%02x", k);
    Serial.print(t);
    if (check) {
      valid = valid && d[i] == k;
      if (!valid) {
	/* Serial.print("Mismtach in byte: "); */
	/* Serial.println(i); */
      }
    }
    d[i] = k;
  }
  Serial.println("");
  if (!check) {
    k = BT.read();
    sprintf(t, "%02x", k);
    Serial.print("left ");
    Serial.println(t);
  }
  return valid;
}

void loop() {
  while (BT.available() > 0) {
    r = BT.read();
    if (r == 1) {
      delay(500);
      Serial.write("sending data...");
      fill_random_bytes(data, NONCE_LENGTH);
      dump_byte_array(data, NONCE_LENGTH);

      BT.write(FIRST);
      BT.write((const char *)data, NONCE_LENGTH);
      delay(200);
      Serial.println("sent.");
    } else if (r == 2){
      ok = read_bytes(data, NONCE_LENGTH, true);
      if (!ok) {
      	Serial.println("Error reading nonce");
      	Serial.print("got ");
      	dump_byte_array(data, NONCE_LENGTH);
      	goto notify;
      }
      ok = read_bytes(signature, 64, false);
      if (!ok) {
      	Serial.println("Error reading signature");
      	goto notify;
      }
      ok = uECC_verify(public_key, data, signature);
      if (!ok) {
      	dump_byte_array(signature, 64);
      	Serial.println("Error verifying signature");
      }
    notify:
      // clear input
      while (BT.available() > 0) {
    	BT.read();
      }
      BT.write(SECOND);
      BT.write(ok);
      fill_circle(ok?0:255, ok?255:0, 0, 20);
      if (ok) {
      	music_ok();
      } else {
      	music_bad();
      }
      delay(2000);
      fill_circle(0, 0, 0);
    } else {
      Serial.write(r);
    }
  }

  while (Serial.available() > 0) {
    r = Serial.read();
    Serial.write(r);
    if (r == 'j') {
      // Here you should use the mac address of your bluetooth chip
      BT.write("AT+BIND=b86c,e8,3b2b48\r\n");
      BT.write("AT+LINK=b86c,e8,3b2b48\r\n");
      fill_circle(255, 255, 255);
    } else {
      BT.write(r);
    }
  }

}
