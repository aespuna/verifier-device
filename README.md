# Verifier device

This code is for a PoC device that without having a direct connection to the internet, verifies with a trusted server wether the untrusted user has permission or not.

## How does it work?

1. The device generates a random nonce
2. Sends it by bluetooth to the untrusted client
3. The client sends the nonce and some other optional information to the server
4. The server then decides to sign or not sign the once with its private key, probably based on the optional information
5. The client must present the signed nonce to the device via bluetooth
6. The device checks if the signature is valid using the server's public key
7. If everything is ok, green leds and a specific beep is presented to the user. Otherwise red lights are showed.

This uses ECC instead of RSA because this way it can run the check on an Arduino nano under 1 second

## Graphical demo

It looks like this when paired with the app:

![Demo](demo.gif)
